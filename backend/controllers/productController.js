import asyncHandler from 'express-async-handler'
import Product from '../models/productModel.js'

/** 
    @desc get all products
    @route GET api/product/products
    @access public
**/

const getProduct = asyncHandler(async(req,res)=>{
    const products = await Product.find({})
    res.json(products)
})

/**
    @desc get single product
    @route get api/product/:id
    @access public

**/

const getProductById = asyncHandler(async(req,res) => {
    const product = await Product.findById(req.params.id)
    
    if(product){
        res.json(product)
    }else{
        res.status(404)
        throw new Error(`Error not found`);
    }
})

export {getProduct, getProductById}
