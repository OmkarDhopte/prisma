import jwt from 'jsonwebtoken'
import AsyncHandler from 'express-async-handler'
import Users from '../models/userModel.js'

const protect = AsyncHandler(async function (req,res, next) {
    let Token

    if (
        req.headers.authorization && 
        req.headers.authorization.startsWith('Bearer')
    ) {
        try {
            Token = req.headers.authorization.split(' ')[1]
            const decoded = jwt.verify(Token,process.env.JWT_SECRET)
            req.user = await Users.findById(decoded.id).select('-password')
            next()
        } catch (error) {
            console.error(error);
			throw new Error('Not authorized, token failed');
        }
    } else {
		throw new Error('No token found');
	}
})

export { protect }

