const products = [
	{
		name: 'Chocolate Cake',
		image: '/images/ChocolateCake.jpeg',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'Prisma',
		category: 'Food',
		price: 1000,
		countInStock: 22,
		rating: 2.6,
		numReviews: 4,
	},
	{
		name: 'Red Velvet Cake',
		image: '/images/RedVelvetCake.jpeg',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'Prisma',
		category: 'Food',
		price: 2500,
		countInStock: 42,
		rating: 2.6,
		numReviews: 2,
	},
	{
		name: 'Carrot Cake',
		image: '/images/CarrotCake.png',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'Prisma',
		category: 'Food',
		price: 2000,
		countInStock: 18,
		rating: 4.5,
		numReviews: 2,
	},
	{
		name: 'Coconut Cake',
		image: '/images/CoconutCake.png',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'Prisma',
		category: 'Food',
		price: 1800,
		countInStock: 8,
		rating: 4.8,
		numReviews: 12,
	},
	{
		name: 'Lemon Cake',
		image: '/images/LemonCake.png',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'Prisma',
		category: 'Food',
		price: 1500,
		countInStock: 6,
		rating: 4.9,
		numReviews: 8,
	},
	{
		name: 'Marble Cake',
		image: '/images/MarbleCake.png',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'Prisma',
		category: 'Food',
		price: 1600,
		countInStock: 21,
		rating: 4.1,
		numReviews: 3,
	},
	{
		name: 'Strawberry Cake',
		image: '/images/StrawberryCake.png',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'Prisma',
		category: 'Food',
		price: 1500,
		countInStock: 25,
		rating: 3.4,
		numReviews: 3,
	},
	{
		name: 'Vanilla Cake',
		image: '/images/VanillaCake.png',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'Prisma',
		category: 'Food',
		price: 2500,
		countInStock: 0,
		rating: 4.2,
		numReviews: 5,
	},
];

export default products;
