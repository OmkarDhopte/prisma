import {
	Button,
	Flex,
	Grid,
	Heading,
	Image,
	Select,
	Text,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link as RouterLink, useParams, useNavigate } from 'react-router-dom';
import { listProductDetails } from '../actions/productActions';
import Loader from '../components/loader';
import Message from '../components/Message';
import Rating from '../components/Rating';

const ProductScreen = () => {
	const navigate = useNavigate()
	const dispatch = useDispatch();

	const { id } = useParams();

	const [qty, setQty] = useState(1);

	const productDetails = useSelector((state) => state.productDetails);
	const { loading, error, product } = productDetails;

	useEffect(() => {
		dispatch(listProductDetails(id));
	}, [id]);

	const addToCartHandler = function () {
		navigate(`/cart/${id}?qty=${qty}`)
	}

	const scale = `scale(1.02)`
	return (
		<>
			<Flex mb='5'>
				<Button as={RouterLink} to='/' backgroundColor='#FED7E2' _hover={{bgColor:'#F687B3', textColor:'#FFF5F7'}}>
					Go Back
				</Button>
			</Flex>

			{loading ? (
				<Loader />
			) : error ? (
				<Message type='error'>{error}</Message>
			) : (
				<Grid templateColumns='5fr 4fr 3fr' gap='10'>
					{/* Column 1 */}
					<Image src={product.image} alt={product.name} borderRadius='md' _hover={{transform:scale}}/>

					{/* Column 2 */}
					<Flex direction='column'>
						<Heading as='h5' fontSize='base' color='pink.400'>
							{product.brand}
						</Heading>

						<Heading as='h2' fontSize='4xl' mb='4'>
							{product.name}
						</Heading>

						<Rating
							color='yellow.500'
							value={product.rating}
							text={`${product.numReviews} reviews`}
						/>

						<Heading
							as='h5'
							fontSize='4xl'
							fontWeight='bold'
							color='pink.500'
							mt='5'
							mb='4'>
							₹{product.price}
						</Heading>

						<Text>{product.description}</Text>
					</Flex>

					{/* Column 3 */}
					<Flex direction='column'>
						<Flex justifyContent='space-between' py='2'>
							<Text>Price: </Text>
							<Text fontWeight='bold'>₹{product.price}</Text>
						</Flex>

						<Flex justifyContent='space-between' py='2'>
							<Text>Status: </Text>
							<Text fontWeight='bold'>
								{product.countInStock > 0 ? 'In Stock' : 'Not available'}
							</Text>
						</Flex>

						{product.countInStock > 0 &&(
							<Flex justifyContent='space-between' py='2'>
								<Text>Qty: </Text>
								<Select
									value={qty}
									onChange={(e) => setQty(e.target.value)}
									width='30%'>
									{[...Array(product.countInStock).keys()].map((i) => (
										<option key={i + 1}>{i + 1}</option>
									))}
								</Select>
							</Flex>
						)}
						<Button
							bg='#B83280'
							textColor='#FED7E2'
							my='2'
							textTransform='uppercase'
							letterSpacing='wide'
							isDisabled={product.countInStock === 0}
							onClick={addToCartHandler}
							_hover={{bgColor:'#F687B3', textColor:'#FFF5F7'}}>
							Add to cart
						</Button>
					</Flex>
				</Grid>
			)}
		</>
	);
};

export default ProductScreen;
