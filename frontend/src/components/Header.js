import { 
	Box,
	Button, 
	Flex, 
	Heading, 
	Icon, 
	Link,
	Menu,
	MenuButton,
	MenuItem,
	MenuList
} from '@chakra-ui/react';
import { useState } from 'react';
import { HiOutlineMenuAlt3, HiShoppingBag, HiUser } from 'react-icons/hi'
import { IoChevronDown } from 'react-icons/io5'
import { useDispatch,useSelector } from 'react-redux'
import { Link as RouterLink,useNavigate } from 'react-router-dom'
import { Logout } from '../actions/userAction'
import HeaderMenuItem from './HeaderMenuItem';

const Header = () => {
	const dispatch = useDispatch()
	const navigate = useNavigate()

	const [show, setShow] = useState(false)

	const userLogin = useSelector((state)=>state.userLogin)
	const { userInfo } = userLogin

	const logoutHandler = () => {
		dispatch(Logout())
		navigate('/login')
	}

	return (
		<Flex
			as='header'
			align='center'
			justifyContent='space-between'
			wrap='wrap'
			py='6'
			px='6'
			bgColor='#D53F8C'
			w='100%'
			pos='fixed'
			top='0'
			left='0'
			zIndex='10'>
			{/* Logo */}
			<Link as={RouterLink} to='/'>
				<Heading
					as='h1'
					color='#FFFFF0'
					fontWeight='bold'
					size='md'
					letterSpacing='wide'>
					Prisma
				</Heading>
			</Link>

			{/* Menu Icon */}
			<Box
				display={{ base: 'block', md: 'none' }}
				onClick={() => setShow(!show)}>
				<Icon as={HiOutlineMenuAlt3} color='#FAF089' w='6' h='6' />
			</Box>

			{/* Menu */}
			<Box
				display={{ base: show ? 'block' : 'none', md: 'flex' }}
				width={{ base: 'full', md: 'auto' }}
				mt={{ base: '5', md: '0' }}
				>
				<HeaderMenuItem url='/cart' icon={HiShoppingBag}>
					Cart
				</HeaderMenuItem>

				{
					userInfo ? (
						<Menu>
							<MenuButton
							as={Button}
							rightIcon={<IoChevronDown />}
							_hover={{ textDecor: 'none', opacity: '0.7' }}>
							{userInfo.name}
						</MenuButton>
						<MenuList>
							<MenuItem as={RouterLink} to='/profile'>
								Profile
							</MenuItem>
								<MenuItem onClick={logoutHandler}>Logout</MenuItem>
							</MenuList>
						</Menu> ) :
						(
							<HeaderMenuItem url='/login' icon={HiUser}>
								Login
							</HeaderMenuItem>
						)
				}
			</Box>
		</Flex>
	);
};

export default Header;
