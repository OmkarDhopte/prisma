import { Box, Flex, Heading, Image, Link, Text } from '@chakra-ui/react'
import { Link as RouterLink } from 'react-router-dom';
import Rating from './Rating';

const ProductCard = ({ product }) => {

	const scale = `scale(1.03)`

	return (
		<Link
			as={RouterLink}
			to={`/product/${product._id}`}
			_hover={{ textDecor: 'none' }}>
			<Box
				borderRadius='10px'
				bgColor='#FEFCBF'
				width='full'
				_hover={{ shadow: 'md',transform:scale }}>
				<Image
					src={product.image}
					alt={product.name}
					w='full'
					h='430px'
					objectFit='cover'
					borderRadius='10px'
				/>
				<Flex py='5' px='4' direction='column' justifyContent='space-between'>
					<Heading as='h4' fontSize='lg' mb='3' textColor='#C05621'>
						{product.name}
					</Heading>
					<Flex alignItems='center' justifyContent='space-between'>
						<Rating value={product.rating} color='yellow.500' />
						<Text fontSize='2xl' fontWeight='bold' color='blue.600'>
							₹{product.price}
						</Text>
					</Flex>
				</Flex>
			</Box>
		</Link>
	);
};

export default ProductCard;
