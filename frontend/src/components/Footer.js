import { Flex, Text } from '@chakra-ui/react';

const Footer = () => {
	return (
		<Flex as='footer' justifyContent='center' py='5' backgroundColor='#FBD38D' textColor='#B83280'>
			<Text>
				Copyright {new Date().getFullYear()}. Prisma. All Rights Reserved.
			</Text>
		</Flex>
	);
};

export default Footer;
